//
//  SceneDelegate.h
//  FormatArabicNumerals
//
//  Created by eiop on 2021/7/27.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

