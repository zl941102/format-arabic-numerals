//
//  UILabel+Category.h
//  Test
//
//  Created by eiop on 2021/7/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (Category)
- (void)setNumberColor:(UIColor *)color;
@end

NS_ASSUME_NONNULL_END
