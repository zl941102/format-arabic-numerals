//
//  UILabel+Category.m
//  Test
//
//  Created by eiop on 2021/7/26.
//

#import "UILabel+Category.h"
#import "NSString+Category.h"

@implementation UILabel (Category)
- (void)setNumberColor:(UIColor *)color {
    if (!self.text.length) {
        return;
    }
    NSString *resultString = @"";//拼接字符串，最终得到一个逗号分割开数字的字符串，例:1,23,456,7.89
    for (NSUInteger index = 0; index < self.text.length; index ++) {
        NSString *currentString = @"";//当前字符
        if (self.text.length > 0) {
            currentString = [self.text substringWithRange:NSMakeRange(index, 1)];
        }
        NSString *lastString = @"";//当前字符的上一个字符
        if (index > 0) {
            lastString = [self.text substringWithRange:NSMakeRange(index-1, 1)];
        }
        NSString *nextString = @"";//当前字符的下一个字符
        if (index < self.text.length-1) {
            nextString = [self.text substringWithRange:NSMakeRange(index+1, 1)];
        }
        NSLog(@"上一个字符：%@ 当前字符：%@ 下一个字符：%@",currentString,lastString,nextString);
        
        BOOL isInt = NO;//用于记录当前字符是否符合浮点型条件
        if (index > 0) {
            if ([currentString isEqualToString:@"."] && [lastString isPureInt] && [nextString isPureInt]) {//判断当前字符是小数点，且上一个字符是整型，下一个字符也是整型
                isInt = YES;
            }
        }
        
        if ([currentString isPureInt]) {//当前字符为整型情况下，可继续执行
            if (!resultString.length) {//为空的情况下，拼接当前字符串
                resultString = [NSString stringWithFormat:@"%@%@",resultString,currentString];
            }
        } else {
            if ([lastString isPureInt]){
                if (isInt) {//当前字符是小数点并且前后一个字符都是整型的情况下
                    NSString *lastObject = [resultString componentsSeparatedByString:@","].lastObject;//找到最后一个元素
                    if ([lastObject rangeOfString:@"."].location != NSNotFound) {//判断最后一个元素是否包含小数点，做如下的字符串拼接
                        resultString = [NSString stringWithFormat:@"%@,%@",resultString,nextString];////拼接逗号和下一个字符
                    }else{
                        resultString = [NSString stringWithFormat:@"%@%@%@",resultString,currentString,nextString];//拼接当前字符和下一个字符
                    }
                }else{
                    resultString = [NSString stringWithFormat:@"%@,",resultString];//当前字符不为整型，并且上一个字符是数字，拼接一个逗号将其分隔开
                }
            }
        }
        
        if ([nextString isPureInt] && !isInt) {//如果下一个字符是整型或者当前字符 符合浮点型条件
            resultString = [NSString stringWithFormat:@"%@%@",resultString,nextString];
        }
    }
    
    NSLog(@"拼接后的字符串：%@",resultString);
    
    NSMutableArray *numberArray = [NSMutableArray arrayWithArray:[resultString componentsSeparatedByString:@","]];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.text];
    [attributedString addAttribute:NSForegroundColorAttributeName value:self.textColor range:NSMakeRange(0, self.text.length - 1)];
    
    NSMutableArray *subscriptArray = [NSMutableArray array];
    
    NSLog(@"开始找连着的数字：");
    for (NSString *numberString in numberArray) {
        NSRange range = [self.text rangeOfString:numberString];
        [attributedString addAttribute:NSForegroundColorAttributeName value:color range:range];
        [subscriptArray addObject:@{@"string":numberString,@"rangeLocation":@(range.location),@"rangeLength":@(range.length)}];
    }
    
    NSLog(@"最后的数据%@",subscriptArray);
    
    self.attributedText = attributedString;
}

@end
