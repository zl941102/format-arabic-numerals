//
//  NSString+Category.h
//  Test
//
//  Created by eiop on 2021/7/27.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Category)

/// 将阿拉伯数字处理成中文数字
/// @param company 是否需要单位（个十百千万....）
- (NSMutableAttributedString *)convertArabicNumeralsForChineseNumerals:(BOOL)company;

/// 判断一个字符串是否为阿拉伯数字
- (BOOL)isPureInt;

@end

NS_ASSUME_NONNULL_END
