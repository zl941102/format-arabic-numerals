//
//  ViewController.m
//  Test
//
//  Created by eiop on 2021/7/26.
//

#import "ViewController.h"
#import "UILabel+Category.h"
#import "NSString+Category.h"

@interface ViewController ()<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *resTextView;
@property (weak, nonatomic) IBOutlet UITextView *inputTextView;
@property (weak, nonatomic) IBOutlet UISwitch *companySwitch;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //[self.label setNumberColor:UIColor.blueColor];
    [self.inputTextView becomeFirstResponder];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@"在此输入"]) {
        textView.text = @"";
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    if (!textView.text.length) {
        self.resTextView.text = @"结果显示在此处";
    }else{
        self.resTextView.attributedText = [self.inputTextView.text convertArabicNumeralsForChineseNumerals:self.companySwitch.on];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (!textView.text.length) {
        textView.text = @"在此输入";
    }else{
        [self textViewDidChange:self.inputTextView];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (IBAction)switchChanged:(UISwitch *)sender {
    [self textViewDidChange:self.inputTextView];
}

@end
